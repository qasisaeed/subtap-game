﻿using UnityEngine;
using System.Collections;

public class Magnet : MonoBehaviour {
	int counter;
	AudioSource audio;
	public AudioClip impact;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
	}
	void OnTriggerEnter2D(Collider2D coll){
		
		if (coll.transform.tag == "coin") {
			PlayerPrefs.SetInt ("tempcoin", PlayerPrefs.GetInt("tempcoin")+ 1 );
			audio.PlayOneShot (impact, 1.5f);
//			counter = PlayerPrefs.GetInt ("magnettempcoin") +1;
//			PlayerPrefs.SetInt ("magnettempcoin", counter);
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
