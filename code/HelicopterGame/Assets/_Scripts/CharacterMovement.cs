﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CharacterMovement : MonoBehaviour {
	public Image bar;
	public Image distancebar;
	private float maxtime= 120f;
	private float mintime=0f;
	private float distancetime= 120f;
	private float distancemintime=0f;
	public float speed = 2;
	private Animator blast;
	public float force = 100;
	public Text coin;
	Rigidbody2D _rigidBody;
	public GameObject gameOver;
	float counters;
	public GameObject spark;
	public GameObject fuelcol;
	Animator animator;
	public GameObject pause;
	public AudioClip impact;
	public AudioClip coinsound;
	AudioSource audio;
	public GameObject bubble;
	private bool shieldCheck = false;
	public GameObject magnet;
	public AudioClip chestSound;
	public AudioClip fuelSound;
	public GameObject levelcompleted;
	public GameObject Damage;
	public UIScript Uiscrip;
	public PluginManager _plugins;
	public GameObject player;
	public GameObject animation;
	public GameObject start1;
	public GameObject start2;
	public GameObject three;
	public GameObject two;
	public GameObject one;
	public GameObject propalersound;
	public GameObject uiscript;

	// Use this for initialization
	void Start () {
		
		Time.timeScale = 0;
		audio = GetComponent<AudioSource>();
		animator = GetComponent<Animator>();
		mintime = maxtime;
		InvokeRepeating ("decreasetime", 0f, 0.3f);
		InvokeRepeating ("distancetimes",0f, 1f);
		counters = 20.0f;
		//Time.timeScale = 1;
		PlayerPrefs.SetInt ("tempcoin", 0);
		PlayerPrefs.SetInt ("tempGift", 0);
//		PlayerPrefs.SetInt ("coinss", 0);
//		PlayerPrefs.SetInt ("chest", 0);
		_rigidBody = GetComponent<Rigidbody2D>();
		_rigidBody.velocity = Vector2.right * speed;
	}

	void distancetimes()
	{
		
		distancemintime += 1f;

		float calctime = distancemintime/distancetime;
		distime (calctime);
	}
	void distime(float distimes){
		distancebar.fillAmount = distimes;

		if (distancemintime ==120f) {
		Time.timeScale = 0;

			PlayerPrefs.SetInt ("coinss", PlayerPrefs.GetInt ("tempcoin") + PlayerPrefs.GetInt ("coinss"));
			PlayerPrefs.SetInt ("chest", PlayerPrefs.GetInt ("tempGift") + PlayerPrefs.GetInt ("chest"));
					Uiscrip.fbManager.PostScore (PlayerPrefs.GetInt ("coinss"));

			levelcompleted.SetActive (true);
		}
	}
	void decreasetime(){
		mintime -= 0.5f;
		float calctime = mintime / maxtime;
		Settime (calctime);
	}
	void Settime(float timeup)
	{
		bar.fillAmount = timeup;
		if (timeup < 0) {
			
			Time.timeScale = 0;
			PlayerPrefs.SetInt ("coinss", PlayerPrefs.GetInt ("tempcoin") + PlayerPrefs.GetInt ("coinss"));
			PlayerPrefs.SetInt ("chest", PlayerPrefs.GetInt ("tempGift") + PlayerPrefs.GetInt ("chest"));
			Uiscrip.fbManager.PostScore (PlayerPrefs.GetInt ("coinss"));
			gameOver.SetActive (true);
			_plugins.ShowChartboostAd ();
		}
	}
	// Update is called once per frame
	string coun;
	float timeLeft = 50f;
	void Update () {
		
		timeLeft -= 0.5f;

		if (timeLeft == 49) {
			three.SetActive (true);
		}
		if (timeLeft == 30) {
			three.SetActive (false);
			two.SetActive (true);
		}
		if (timeLeft == 10) {
			two.SetActive (false);
			one.SetActive (true);
		}
		if(timeLeft == 0)
		{
			one.SetActive (false);

				propalersound.SetActive (true);

			Time.timeScale = 1;
		}

		//		counters -= Time.deltaTime;
//		bar.fillAmount = counters;
//		if (counters < 0) {
//			Time.timeScale = 0;
//			gameOver.SetActive (true);
//		}
		coun = PlayerPrefs.GetInt ("tempcoin").ToString ();
		coin.text = coun;
		// Upward Force
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown(0)){
//			_rigidBody.velocity = Vector2.zero;
			_rigidBody.AddForce(Vector2.up * force);
		}

	}
	void blastAni (){
		
		Time.timeScale = 0;
//		animation.SetActive (false);
//		Uiscrip.player.SetActive (false);
//
		Destroy (gameObject);
		Debug.Log ("tempcoin");
		Debug.Log (PlayerPrefs.GetInt("tempcoin"));
		PlayerPrefs.SetInt ("coinss", PlayerPrefs.GetInt ("tempcoin") + PlayerPrefs.GetInt ("coinss"));
		PlayerPrefs.SetInt ("chest", PlayerPrefs.GetInt ("tempGift") + PlayerPrefs.GetInt ("chest"));
		Debug.Log ("coin");
		Debug.Log (PlayerPrefs.GetInt("coinss"));
		Uiscrip.fbManager.PostScore (PlayerPrefs.GetInt ("coinss"));
		transform.localScale = new Vector3 (0.05f, 0.05f, 0.05f);
		//gameObject.transform.localScale = new Vector3(0.05f, 0.05f,0.05f);
		if (distancemintime < 50f)
		{
			start1.SetActive (true);
		}
		if (distancemintime > 50f)
		{
			start1.SetActive (true);
			start2.SetActive (true);
		}
		gameOver.SetActive (true);
		_plugins.showInterstitial ();
		pause.SetActive (false);			
	}
	void blastrocks(){
		
		Time.timeScale = 0;
//		animation.SetActive (false);
//		Uiscrip.player.SetActive (false);
		Destroy (gameObject);
		PlayerPrefs.SetInt ("coinss", PlayerPrefs.GetInt ("tempcoin") + PlayerPrefs.GetInt ("coinss"));
		PlayerPrefs.SetInt ("chest", PlayerPrefs.GetInt ("tempGift") + PlayerPrefs.GetInt ("chest"));
		Uiscrip.fbManager.PostScore (PlayerPrefs.GetInt ("coinss"));
		if (distancemintime < 50f)
		{
			start1.SetActive (true);
		}
		if (distancemintime > 50f)
		{
			start1.SetActive (true);
			start2.SetActive (true);
		}
		gameOver.SetActive (true);
		_plugins.showInterstitial ();
		pause.SetActive (false);

	}
		int counter;
	int damage=0;
	void damagereturn(){
		animator.SetBool ("damge", false);
		animator.SetBool ("damgerun", true);
	}
	void OnCollisionEnter2D(Collision2D col){
		
		if (shieldCheck == false) {
			if (col.transform.tag == "mines") {
				Damage.SetActive (true);
				damage++;

				audio.PlayOneShot (impact, 8F);
				animator.SetBool ("damge", true);
				Invoke ("damagereturn", 0.3f);

					//animation.SetActive (true);
					//
				if (damage ==2) {
					animator.SetBool ("minesblast", true);
					Invoke ("blastAni", 0.5f);
				}
				
			}

			if (col.transform.tag == "rocks") {
				damage++;
				Damage.SetActive (true);
				animator.SetBool ("damge", true);
				audio.PlayOneShot (impact, 8F);
				Invoke ("damagereturn", 0.3f);
//					animation.SetActive (true);
					
				if (damage == 2) {
					animator.SetBool ("PlayerDead", true);
					Invoke ("blastrocks", 0.5f);
				}

			}

			if (col.transform.tag == "Missiles") {
				damage++;
				Damage.SetActive (true);
				animator.SetBool ("damge", true);
				audio.PlayOneShot (impact, 8F);
				Invoke ("damagereturn", 0.3f);
				if (damage == 2) {
					
					//animation.SetActive (true);
					animator.SetBool ("minesblast", true);
					Invoke ("blastAni", 0.3f);
				}
			}
		}

	}
	void buble(){
		bubble.SetActive (false);
		shieldCheck = false;
	}
	void Sheild(){
		
		bubble.SetActive (true);
		shieldCheck = true;
		Invoke ("buble", 10);
	}
	void flarepark(){
		fuelcol.SetActive (false);
	}
	void OnTriggerEnter(Collider coll){
		Debug.Log ("Col"+ coll.name);

	}
	void coinss(){
		spark.SetActive(false);

	}
	void magnetTime(){
		magnet.SetActive (false);
	}
	int gift=0;
	int coint=0;
	void OnTriggerEnter2D(Collider2D coll){
		
		if (coll.transform.tag == "coin") {
			spark.SetActive (true);
			audio.PlayOneShot (coinsound, 1.5f);
			//counter = PlayerPrefs.GetInt ("coin") +1;
			coint++;
			PlayerPrefs.SetInt ("tempcoin", PlayerPrefs.GetInt("tempcoin")+ 1 );

			Invoke ("coinss", 1f);
		}
		if (coll.transform.tag == "fuel") {
			fuelcol.SetActive (true);
			//float i = maxtime % mintime;
			//mintime = mintime + i;
			audio.PlayOneShot (fuelSound, 3F);
			mintime = 120;
			Invoke ("flarepark", 0.6f);
			Destroy (coll.gameObject);
		}

		if (coll.transform.tag == "gift") {
			gift++;
			PlayerPrefs.SetInt ("tempGift", gift);
			audio.PlayOneShot (chestSound, 3F);
			spark.SetActive (true);
			Destroy (coll.gameObject);
			Invoke ("coinss", 1f);
		}
		if (coll.transform.tag == "Bubble") {
			audio.PlayOneShot (fuelSound, 3F);
			Destroy (coll.gameObject);
			Sheild ();
		}
		if (coll.transform.tag == "magnet") {
			audio.PlayOneShot (fuelSound, 3F);
			magnet.SetActive (true);
			Destroy (coll.gameObject);
			Invoke ("magnetTime", 8f);

		}

	}

}
