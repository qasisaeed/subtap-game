﻿using UnityEngine;
using System.Collections;

public class minesblast : MonoBehaviour {
	//public GameObject blast;
	Animator animator;
	// Use this for initialization
	void Start () {
		
		animator = GetComponent<Animator>();
	}
	void OnCollisionEnter2D(Collision2D col){
		if (col.transform.tag == "sub") {
			animator.SetBool ("MinesBlast", true);
			//blast.SetActive (true);
			Destroy (gameObject,0.1f);
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
