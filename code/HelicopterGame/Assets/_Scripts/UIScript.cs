﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class UIScript : MonoBehaviour {
	public GameObject GamePlay;
	public GameObject canvasUI;
	public GameObject btn1;
	public GameObject btn2;
	public GameObject btn3;
	public GameObject pausebtn;
	//public GameObject SoundOffBtn;
	//public GameObject network;
	public GameObject pauseDialog;
	public GameObject gameplaypanel;
	public Text life;
	public Text dailycoins;
	public GameObject dailybonus;
	public GameObject leaderboard;
	public FacebookManager fbManager;
	public GameObject gameOver;
	public Text[] Scores;
	public Text[] Name;
	public GameObject Audio1;
	public GameObject Audio2;
	public Text TotalCoins;
	public Text totalchest;
	public Text TotalCoinsGO;
	public Text totalchestGO;
	public GameObject live1;
	public GameObject live2;
	public GameObject live3;
	public GameObject live4;
	public GameObject LiveDialog;
	public GameObject player;
	public GameObject exit;
	public GameObject commingsoon;


	Dictionary<string,string> friendsScore = new Dictionary<string, string>();
	// Use this for initialization
	void Start () {
		//Invoke ("countdown", 1f);
		 
		//Invoke ("splashscreen", 3f);
//		if (PlayerPrefs.GetInt("bonus") ==0){
//			//Debug.Log( System.DateTime.Now.Day);
//			PlayerPrefs.SetInt("bonus", System.DateTime.Now.Day);
//		}
//		if (PlayerPrefs.GetInt("bonus") != System.DateTime.Now.Day) {
//			dailybonus.SetActive (true);
//			int coin =  (UnityEngine.Random.Range (100, 200));
//			dailycoins.text = coin.ToString ();
//			PlayerPrefs.SetInt ("coin", PlayerPrefs.GetInt("coin")+coin);
//
//
//			PlayerPrefs.SetInt("bonus", System.DateTime.Now.Day);
//		}

//		if (PlayerPrefs.GetInt("lifes") == 0){
//		PlayerPrefs.SetInt ("lifes", 5);
//		}

		//Debug.Log(System.DateTime.Now.Hour);


		Time.timeScale = 1;
		if (PlayerPrefs.GetInt ("Replay") == 1) {
			canvasUI.SetActive (false);
			gameplaypanel.SetActive (true);
			GamePlay.SetActive (true);
			PlayerPrefs.SetInt ("Replay", 0);
		}

		fbManager.sDelegate += PopulateLeaderBoard;

	}
//	void countdown(){
//		three.SetActive (true);
//		Invoke ("counttwo", 1f);
//	}
//	void counttwo (){
//		three.SetActive (false);
//		two.SetActive (true);
//		Invoke ("countone", 1f);
//	}
//	void countone(){
//		two.SetActive (false);
//		one.SetActive (true);
//		Invoke ("startff", 1f);
//	}

	public void PopulateLeaderBoard(List<object> scoresResponse ){
//		leaderboard.SetActive(true);
		//		fbManager.GetScores();
	foreach(object scoreItem in scoresResponse) 
	{
		// Score JSON format
		// {
		//   "score": 4,
		//   "user": {
		//      "name": "Chris Lewis",
		//      "id": "10152646005463795"
		//   }
		// }

			Dictionary<string,object> entry = (Dictionary<string,object>) scoreItem;
			var user = (Dictionary<string,object>) entry["user"];
			string userName = (string)user["name"];
			string scoreValue = entry["score"].ToString();
			friendsScore.Add (userName, scoreValue);
			Debug.Log ("userName:"+userName+" scoreValue:"+scoreValue);
		//			if (string.Equals(userId, AccessToken.CurrentAccessToken.UserId))
		//			{
		//				
		//			}

		//			structuredScores.Add(entry);

	}
	}
	int counter=0;
	int sound=0;
	public static bool soundcheck;
	public void soundONOFFbtn(){
		if (sound == 0) {
			Audio1.SetActive(false);
			Audio2.SetActive(false);
			soundcheck = false;

			sound++;
		}
		else if (sound == 1) {
			Audio1.SetActive(true);
			Audio2.SetActive(false);
			soundcheck = true;
			sound--;
		}
	}
	public void leaderboardBtnClicked(){
		//gameOver.SetActive (false);
		//Invoke("getscore", 2);
		//fbManager.GetScores ();
		foreach (KeyValuePair<string,string> item in friendsScore){

			Name[counter].text = item.Key; // Name
			Scores[counter].text = item.Value; //Score
			counter++;
		}

		for (int i = counter; i < Scores.Length; i++) {
			Scores [i].transform.parent.gameObject.SetActive (false);
			// Name[i].transform.parent.gameObject.SetActive (false);
		}
		leaderboard.SetActive(true);

//		fbManager.GetScores();
}
	void getscore(){
		
	}
	public void DailyButtonOk (){
		dailybonus.SetActive (false);
	}
	public void playBtn(){
		
		gameplaypanel.SetActive (true);
		canvasUI.SetActive (false);
		GamePlay.SetActive (true);
		Time.timeScale = 0;

	}
	int hit=0;
	public void networkingbtn(){
		
		if (hit == 0) {
			//network.SetActive (true);
			btn1.SetActive (true);
			btn2.SetActive (true);
		//	btn3.SetActive (true);
			hit += 1;
		} else if (hit == 1) {
			btn1.SetActive (false);
			btn2.SetActive (false);
			//btn3.SetActive (false);
			hit -= 1;
		}
	}
	public void LeaderBoardCross(){
		leaderboard.SetActive (false);
		gameOver.SetActive (true);
	}
	public void facebookbtn(){
//		Application.OpenURL ("www.facebook.com");
		fbManager.Initialize();

	}
	public void pause(){
		Time.timeScale = 0;
		Audio1.SetActive (false);
		Audio2.SetActive (false);
		pauseDialog.SetActive (true);
		pausebtn.SetActive (false);
	}
	public void resume(){
		Time.timeScale = 1;
		Audio1.SetActive (true);
		Audio2.SetActive (true);
		pauseDialog.SetActive (false);
		pausebtn.SetActive (true);
	}
	public void mainmenu(){

		Application.LoadLevel ("GamePlayScene");
	}

	public void Restart(){
		PlayerPrefs.SetInt ("Replay", 1);
		Application.LoadLevel ("GamePlayScene");

		//LiveDialog.SetActive (true);
		//player.SetActive(true);

		//Instantiate(player);
//		canvasUI.SetActive (false);
//		//GamePlay.SetActive (true);
//		GamePlay.SetActive (false);
//		GamePlay.SetActive (true);
//		pauseDialog.SetActive (false);
//		gameOver.SetActive (false);
//		Time.timeScale = 1;
	}

	public void Replay(){

		if (PlayerPrefs.GetInt ("lifes") == 1){
			Debug.Log ("no Life");
		}
		else{
		PlayerPrefs.SetInt ("lifes", PlayerPrefs.GetInt("lifes")-1);
		PlayerPrefs.SetInt("minute",System.DateTime.Now.Minute);
		PlayerPrefs.SetInt("hours",System.DateTime.Now.Hour);
		
		//Application.LoadLevel ("GamePlayScene");
		}
	}
	public void moreapp(){
		Application.OpenURL ("https://play.google.com/store?hl=en");
	}
	public void crossbtn(){
		leaderboard.SetActive (false);
	}
	public void liveReplay(){
		Time.timeScale = 1;
		player.SetActive (true);
	}
	// Update is called once per frame

	public void exitYes(){
		Application.Quit ();
	}
	public void exitNo(){
		exit.SetActive (false);
	}
	public void cancel(){
		commingsoon.SetActive (false);
	}
	public void Nextlevel(){
		commingsoon.SetActive (true);
	}

	void Update () {
		if (Input.GetKey ("escape")) {
			exit.SetActive (true);
			}

	
		TotalCoins.text = PlayerPrefs.GetInt ("coinss").ToString();
		totalchest.text = PlayerPrefs.GetInt ("chest").ToString ();
		 TotalCoinsGO.text  = PlayerPrefs.GetInt ("coinss").ToString();
		totalchestGO.text = PlayerPrefs.GetInt ("chest").ToString ();
//		int i = PlayerPrefs.GetInt ("lifes") - 1;
//		life.text =i.ToString();
//		if (PlayerPrefs.GetInt ("minute")+1 <= System.DateTime.Now.Minute) {
//			PlayerPrefs.GetInt("lifes"==2){
//			live1.SetActive (true);
//			}
//			if(PlayerPrefs.GetInt("hours") != System.DateTime.Now.Hour)
//			{
//				live1.SetActive(true);
//				live2.SetActive(true);
//				PlayerPrefs.set
//			}
//			//PlayerPrefs.SetInt ("minute", PlayerPrefs.GetInt ("minute") + 1);

		}
	}


//}
