﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleSpawne : MonoBehaviour {

	[SerializeField]
	public List<GameObject> randomParallexObjects;
	public List<GameObject> Gifts;
	public GameObject fuel;
	public GameObject stars;
    GameObject collectableItem;
	public float ParallaxSpeed=0;
	public float speed;
	protected Vector3 _movement;

	public List<GameObject> ObjectInstances;
	GameObject newloopTrigger;
	// Use this for initialization
	void Start () {

		ObjectInstances = new List<GameObject> ();
		PopulateScene ();


	}

	void DecideCollectableToDrop(){
		int rand = Random.Range (0, 101);

		if(rand <=70){
			collectableItem = Gifts [Random.Range(0,2)];;
		}		
		else if(rand<=80 && rand >70){
			collectableItem = fuel;
		}
		else{
			

				collectableItem = Gifts [0];

		}
	}	

	IEnumerator Loop(){
		while(!newloopTrigger.GetComponent<Renderer>().isVisible){
			yield return new WaitForSeconds (1f);

		}
		PopulateScene ();
		yield return null;
	}	

	void PopulateScene(){
		for(int i=0;i<45;i=i+10){
			int currentObjNum = Random.Range (0, randomParallexObjects.Count);
			GameObject obj = randomParallexObjects[currentObjNum];
			if(Random.Range(0,2)==0){
				DecideCollectableToDrop ();
//				Debug.Log ("Dropping");
				if(Random.Range(0,2)==0) 
					ObjectInstances.Add ((GameObject)Instantiate (collectableItem, new Vector3 (transform.position.x + (i-4) + (Random.Range (0, 2)), transform.position.y + (Random.Range (0, 3)), transform.position.z), collectableItem.transform.rotation));
				else
					ObjectInstances.Add ((GameObject)Instantiate (collectableItem, new Vector3 (transform.position.x + (i-4) + (Random.Range (0, 2)), transform.position.y - (Random.Range (0, 3)), transform.position.z),  collectableItem.transform.rotation));
				}		
			ObjectInstances.Add ((GameObject)Instantiate(obj,new Vector3(transform.position.x+i +(Random.Range(0,5)),transform.position.y,transform.position.z),Quaternion.identity));
			//			ObjectInstances [i].transform.parent = this.transform;
			PerformSomeChanges (currentObjNum);
		}
		newloopTrigger = ObjectInstances [ObjectInstances.Count-1];
		StartCoroutine ("Loop");
	}


	void PerformSomeChanges(int number){
		int randomNum;
		//		if(number >1){
		//			randomNum = 0;//Random.Range (0,3);	
		//		}else{
		//			randomNum = number;
		//		}	
		randomNum = number;
		switch(randomNum){
		default:
//			ObjectInstances[ObjectInstances.Count-1].transform.localScale *= Random.Range(0.3f,1.0f);
			ObjectInstances [ObjectInstances.Count-1].transform.position = new Vector3 (ObjectInstances [ObjectInstances.Count-1].transform.position.x,ObjectInstances [ObjectInstances.Count-1].transform.position.y+Random.Range(0,5),ObjectInstances [ObjectInstances.Count-1].transform.position.z);
			break;
		case 1:
			ObjectInstances [ObjectInstances.Count - 1].transform.localScale = Vector3.one;
			ObjectInstances [ObjectInstances.Count-1].transform.position = new Vector3 (ObjectInstances [ObjectInstances.Count-1].transform.position.x,ObjectInstances [ObjectInstances.Count-1].transform.position.y,ObjectInstances [ObjectInstances.Count-1].transform.position.z);
			break;
		case 0:
			ObjectInstances [ObjectInstances.Count - 1].transform.localScale = Vector3.one;
			ObjectInstances [ObjectInstances.Count-1].transform.position = new Vector3 (ObjectInstances [ObjectInstances.Count-1].transform.position.x,ObjectInstances [ObjectInstances.Count-1].transform.position.y+2f,ObjectInstances [ObjectInstances.Count-1].transform.position.z);
			break;
		}
	}

	float SpeedIncrease = 0f;
	// Update is called once per frame
	void Update () {
		SpeedIncrease += Time.deltaTime;
		Debug.Log (SpeedIncrease);
		if (SpeedIncrease > 50f) {
			ParallaxSpeed = 6;
		}
		if (SpeedIncrease > 90f) {
			ParallaxSpeed = 7;
		}
		_movement = Vector3.left * (ParallaxSpeed / 10) * speed * Time.deltaTime;

//		Debug.Log (ObjectInstances.Count);
		for( int i = 0; i <ObjectInstances.Count;i++){
			if(ObjectInstances[i]!=null){
				ObjectInstances[i].transform.Translate (_movement);
				if (ObjectInstances[i].transform.position.x < -10f){
					Destroy (ObjectInstances[i]);
					ObjectInstances [i] = null;
					ObjectInstances.Remove (ObjectInstances [i]);
					ObjectInstances.TrimExcess ();

//					Debug.Log ("After"+ObjectInstances.Count);
				}
			}
		}


	}
}
