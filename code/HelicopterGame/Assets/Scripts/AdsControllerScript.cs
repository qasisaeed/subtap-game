using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdsControllerScript {

	public static BannerView bannerView;
	public static InterstitialAd interstitial;

	public static void RequestBanner()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-7220895901828564/5251675937";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-7220895901828564/7068678738";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
	}

	public static void RequestInterstitial()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-7220895901828564/6728409134";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-7220895901828564/1022145133";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
	}

	public static void showInterstitial ()
	{
		if (interstitial.IsLoaded ()) {
			interstitial.Show ();
		} else
			Debug.Log ("Add not shown");
	}

	public static void hideBanner()
	{
		bannerView.Hide ();
	}

	public static void showBanner(){
		bannerView.Show ();
	}

}
