﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class AchievementManager : MonoBehaviour {

	public static AchievementManager _instance;
	
	public static AchievementManager Instance
	{
		get {
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<AchievementManager>();
				
				if (_instance == null)
				{
					GameObject container = new GameObject("AchievementManager");
					_instance = container.AddComponent<AchievementManager>();
				}
			}
			
			return _instance;
		}
	}
	// Use this for initialization
	void Start () 
	{
		//GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);

		// LEADERBOARD INTEGRATION
		
		if (!Social.localUser.authenticated) { // THIS WILL SIGN IN THE USER
			
			// Authenticate
			PlayGamesPlatform.Activate(); // THIS WILL INITIALIZE GOOGLE PLAY SERVICES
			
			Social.localUser.Authenticate((bool success) => {
				if (success) {
					Debug.Log ("User Authenticated");
				}  else {
					Debug.Log ("Authentication failed.");
				}
			} );
		}
	}

	public void UnlockScoreAchievement(string s) 
	{
		Debug.Log ("Please unlock my first achievemenst");
		if (Social.localUser.authenticated) {
			Social.ReportProgress (s, 100.0f, ((bool success) =>
			                                                  {
				if (success)
					Debug.Log ("Score10 Achievement Unlocked");
				else
					Debug.Log ("Score10 achievement failed to be unlocked");
			}));
		}
		
	}

//	public void UnlockScore100Achievement() 
//	{
//		Debug.Log ("Please unlock my 2nd achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score100ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Score100 achievement unlocked");
//				else
//					Debug.Log ("Score100 achievement failed to be unlocked");
//			}));
//		}
//		
//	}
//
//	public void UnlockScore150Achievement() 
//	{
//		Debug.Log ("Please unlock my 3rd achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score150ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Score150 achievement unlocked");
//				else
//					Debug.Log ("Score150 achievement failed to be unlocked");
//			}));
//		}
//		
//	}
//
//	public void UnlockScore250Achievement() 
//	{
//		Debug.Log ("Please unlock my 4th achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score250ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Score250 achievement unlocked");
//				else
//					Debug.Log ("Score250 achievement failed to be unlocked");
//			}));
//		}
//		
//	}
//
//	public void UnlockScore500Achievement() 
//	{
//		Debug.Log ("Please unlock my 5th achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score500ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Score500 achievement unlocked");
//				else
//					Debug.Log ("Score500 achievement failed to be unlocked");
//			}));
//		}
//		
//	}
//
//	public void UnlockScore600Achievement() 
//	{
//		Debug.Log ("Please unlock my 6th achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score600ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Score600 achievement unlocked");
//				else
//					Debug.Log ("Score600 achievement failed to be unlocked");
//			}));
//		}
//		
//	}
//
//	public void UnlockScore700Achievement() 
//	{
//		Debug.Log ("Please unlock my 7th achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score700ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Score700 achievement unlocked");
//				else
//					Debug.Log ("Score700 achievement failed to be unlocked");
//			}));
//		}
//		
//	}

//	public void UnlockScore800Achievement() 
//	{
//		Debug.Log ("Please unlock my 8th achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score800ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Score800 achievement unlocked");
//				else
//					Debug.Log ("Score800 achievement failed to be unlocked");
//			}));
//		}
//		
//	}
//
//	public void UnlockScore1000Achievement() 
//	{
//		Debug.Log ("Please unlock my 9th achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score1000ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Score1000 achievement unlocked");
//				else
//					Debug.Log ("Level9 achievement failed to be unlocked");
//			}));
//		}
//		
//	}
//
//	public void UnlockScore2000Achievement() 
//	{
//		Debug.Log ("Please unlock my 10th achievemenst");
//		if (Social.localUser.authenticated) {
//			Social.ReportProgress (Score2000ID, 100.0f, ((bool success) =>
//			                                          {
//				if (success)
//					Debug.Log ("Level10 achievement unlocked");
//				else
//					Debug.Log ("Level10 achievement failed to be unlocked");
//			}));
//		}
//		
//	}


	public void ShowAchievements()
	{
		if (Social.localUser.authenticated) 
		{
			Social.ShowAchievementsUI();
		}
	}



}
